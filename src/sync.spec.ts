import {
  findChangesBetweenDatasets,
  findDeltas,
  getDeltas, makeUpdates,
  skuBatchToInserts,
  skuBatchToRecords,
} from './sync';
import { RecordWithWMS, SkuBatchData, skuBatchUpdate } from './interfaces.util';

describe('sync', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('.skuBatchToInserts', () => {
    it('should return a list of inserts and the log the counts for successes and failures', async () => {
      const logSpy = jest.spyOn(global.console, 'log');

      const data = [
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 25,
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 25,
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-2',
          quantityPerUnitOfMeasure: 1,
        },
        {
          skuBatchId: 'non-existent-id',
        }
      ];

      await expect(
        skuBatchToInserts(
          data.map((d) => d.skuBatchId),
        ),
      ).resolves.toStrictEqual([
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-1)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-2)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)",
        "insert into test_table (col_1, col_2) values (sku-id-1, sku-batch-id-3)"
      ]);

      expect(logSpy).toHaveBeenCalledTimes(1);
      expect(logSpy.mock.calls[0][0]).toStrictEqual(`created inserts [count=12, badSkuBatchRecordCount=1]`)
    });

    it('logs an error and does not generate an insert for "not found" skuBatchIds', async() => {
      const errorSpy = jest.spyOn(global.console, 'error');

      await expect(skuBatchToInserts(['non-existent-id'])).resolves.toStrictEqual([]);
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy.mock.calls[0][0]).toStrictEqual('no records found in app SkuBatch [skuBatchId=non-existent-id}]');
    });
  });

  describe('.skuBatchToRecords', () => {
    it('should return a list of records and the log the counts for successes and failures', async () => {
      const logSpy = jest.spyOn(global.console, 'log');

      const data = [
        {
          skuBatchId: 'sku-batch-id-1',
        },
        {
          skuBatchId: 'non-existent-id',
        }
      ];
  
      const ids: string[] = data.map((d) => d.skuBatchId);
      const expected: RecordWithWMS[] = [
        {
         "isArchived": false,
         "isDeleted": false,
         "quantityPerUnitOfMeasure": 1,
         "skuBatchId": "sku-batch-id-1",
         "skuId": "sku-id-1",
         "warehouseId": "warehouse-1",
         "wmsId": 1234,
          },
          {
         "isArchived": false,
         "isDeleted": false,
         "quantityPerUnitOfMeasure": 1,
         "skuBatchId": "sku-batch-id-1",
         "skuId": "sku-id-1",
         "warehouseId": "warehouse-2",
         "wmsId": 1234,
          },
          {
         "isArchived": false,
         "isDeleted": false,
         "quantityPerUnitOfMeasure": 1,
         "skuBatchId": "sku-batch-id-1",
         "skuId": "sku-id-1",
         "warehouseId": "warehouse-3",
         "wmsId": 1234,
          },
          {
         "isArchived": false,
         "isDeleted": false,
         "quantityPerUnitOfMeasure": 1,
         "skuBatchId": "sku-batch-id-1",
         "skuId": "sku-id-1",
         "warehouseId": "warehouse-4",
         "wmsId": 1234,
          }
        ]

      await expect(skuBatchToRecords(ids)).resolves.toStrictEqual(expected)
      expect(logSpy).toHaveBeenCalledTimes(1);
      expect(logSpy.mock.calls[0][0]).toStrictEqual(`created records [count=4, badSkuBatchRecordCount=1]`)
    });

    it('logs an error and does not generate a record for "not found" skuBatchIds', async() => {
      const errorSpy = jest.spyOn(global.console, 'error');

      await expect(skuBatchToRecords(['non-existent-id'])).resolves.toStrictEqual([]);
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy.mock.calls[0][0]).toStrictEqual('no records found in app SkuBatch [skuBatchId=non-existent-id}]');
    });
  });

  describe('.getDeltas', () => {
    it('should find deltas', async () => {
      // example of the data below:
      // skuBatchIds = ['sku-batch-id-1', 'sku-batch-id-2', 'sku-batch-id-3', 'sku-batch-id-4'];
      // appSkuBatchIds = [...skuBatchIds, 'sku-batch-id-5', 'sku-batch-id-6']; // 5 and 6 are new
      await expect(getDeltas()).resolves.toStrictEqual(['sku-batch-id-5', 'sku-batch-id-6']);
    });
  });

  describe('.findDelta', () => {
    // Define data constant in 'describe' scope to avoid repetitive const assignment in each test scenario
    const appData = [{
      skuBatchId: '1',
      skuId: '1',
      wmsId: '1',
      quantityPerUnitOfMeasure: 5,
      isArchived: false,
      isDeleted: false,
    }];

    it('should pick up changes to quantityPerUnitOfMeasure', async () => {
      const inventoryData = [{
       ...appData[0],
       quantityPerUnitOfMeasure: 10,
      }];

      const deltas: skuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe('quantityPerUnitOfMeasure');
      expect(deltas[0].updates[0].newValue).toBe(5);
    });

    it('should not change the skuId if already set', async () => {
      const inventoryData = [{
        ...appData[0],
        skuId: '2',
      }];

      const deltas: skuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(0);
    });

    it('should pick up change to skuId if not set', async () => {
      const inventoryData = [{
        ...appData[0],
        skuId: null,
      }];

      const deltas: skuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe('skuId');
      expect(deltas[0].updates[0].newValue).toBe('1');
    });

    it('should pick up change to wmsId', async () => {
      const inventoryData = [{
        ...appData[0],
        wmsId: '2',
      }];

      const deltas: skuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe('wmsId');
      expect(deltas[0].updates[0].newValue).toBe('1');
    });

    it('should find changes between datasets', async () => {
      await expect(
        findChangesBetweenDatasets(),
      ).resolves.toStrictEqual([
        {
          "isArchived": false,
          "isDeleted": true,
          "quantityPerUnitOfMeasure": 1,
          "skuBatchId": "sku-batch-id-5",
          "skuId": "sku-id-2",
          "wmsId": "1238",
        },
        {
          "isArchived": true,
          "isDeleted": false,
          "quantityPerUnitOfMeasure": 1,
          "skuBatchId": "sku-batch-id-6",
          "skuId": "sku-id-3",
          "wmsId": "1239",
        },
      ]);
    });

    it('should log console warnings for count mismatches', async () => {
      const warnSpy = jest.spyOn(global.console, 'warn');
      
      await findChangesBetweenDatasets();

      expect(warnSpy).toHaveBeenCalled();
      expect(warnSpy).toHaveBeenCalledTimes(3);
      expect(warnSpy.mock.calls[0][0]).toStrictEqual("Warning! The following SkuBatches are present in Inventory db but missing from App db: sku-batch-id-7 sku-batch-id-8");
      expect(warnSpy.mock.calls[1][0]).toStrictEqual("cannot find matching inventory record! [skuBatchId=sku-batch-id-7]")
      expect(warnSpy.mock.calls[2][0]).toStrictEqual("cannot find matching inventory record! [skuBatchId=sku-batch-id-8]")
    })
  });

  describe('.makeUpdates', () => {
    it('should return updated skuBatchData based on provided delta', () => {
      const delta: skuBatchUpdate =
        {
          skuBatchId: "sku-batch-id-1",
          updates: [
            {
              field: "quantityPerUnitOfMeasure",
              newValue: 100
            },
            {
              field: "isArchived",
              newValue: true
            },
            {
              field: "wmsId",
              newValue: null
            },
            {
              field: "skuId",
              newValue: "joe-id"
            }
          ],
        }
      const result: SkuBatchData = makeUpdates(delta)
      const expected =  {
        "isArchived": true,
        "quantityPerUnitOfMeasure": 100,
        "skuBatchId": "sku-batch-id-1",
        "skuId": "joe-id",
        "wmsId": null
      };

      expect(result).toStrictEqual(expected);
    });
  })
});
