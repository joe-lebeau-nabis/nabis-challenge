import {
  WMSWarehouseMeta,
  inventoryUpdate,
  RecordWithWMS,
  SkuBatchData,
  SkuBatchToSkuId,
  skuBatchUpdate,
  InventoryAggregateApiRequestBody,
  InventoryApiRequestBody,
} from './interfaces.util';
import {
    appData,
    appSkuBatchData, appSkuBatchDataForSkuBatchIds,
    skuBatchIdsFromInventoryDb,
    skuBatchIdsFromAppDb,
    warehouseData
} from "./db/data";
import { PostInventory, PostInventoryAggregate, PutInventory, PutInventoryAggregate } from './api/api';
import { insertify } from './db/sql.util';

const logger = console;

/**
 * Create a list of records for a skuBatch record that maps skuBatchId + warehouseId
 * @param skuBatchRecord
 */
const makeWarehouseRecordsForSkuBatchRecord = (skuBatchRecord: SkuBatchToSkuId): RecordWithWMS[] => {
  return warehouseData.map(
    (warehouse: WMSWarehouseMeta): RecordWithWMS => ({
        skuBatchId: skuBatchRecord.skuBatchId,
        skuId: skuBatchRecord.skuId,
        wmsId: skuBatchRecord.wmsId,
        quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure ?? 1,
        isArchived: skuBatchRecord.isArchived,
        isDeleted: skuBatchRecord.isDeleted,
        warehouseId: warehouse.warehouseId,
      }),
  );
};

/**
 * Converts a list of skuBatchIds from the app db into an insert to inventory.
 * Note: I rewrote this function as "skuBatchToRecords" to meet the use API use case in Task 5, but
 * am leaving this implementation in place to reflect my bugfix to get the existing unit tests passing
 * @param skuBatchIdsToInsert
 */
export async function skuBatchToInserts(skuBatchIdsToInsert: string[]): Promise<string[]> {
  const badSkuBatchCounter = { count: 0 };

  // create our inserts
  const inserts: string[] = skuBatchIdsToInsert
    .reduce((arr: RecordWithWMS[], skuBatchId: string): RecordWithWMS[] => {
      const skuBatchRecordFromAppDb: SkuBatchToSkuId | undefined = appData.find(
        (skuBatchToSkuId: SkuBatchToSkuId): boolean => skuBatchToSkuId.skuBatchId === skuBatchId,
      );

      if (!skuBatchRecordFromAppDb) {
        logger.error(`no records found in app SkuBatch [skuBatchId=${skuBatchId}}]`);
        badSkuBatchCounter.count += 1;
        return arr;
      }

      arr.push(...makeWarehouseRecordsForSkuBatchRecord(skuBatchRecordFromAppDb));
      return arr;
    }, [])
    .map(insertify);

  logger.log(`created inserts [count=${inserts.length}, badSkuBatchRecordCount=${badSkuBatchCounter.count}]`);

  return inserts;
}


/**
 * Converts a list of skuBatchIds from the app db into records to be sent to inventory
 * @param skuBatchIdsToCreate
 */
export async function skuBatchToRecords(skuBatchIdsToCreate: string[]): Promise<RecordWithWMS[]> {
  const badSkuBatchCounter = { count: 0 };

  // create our records
  const records: RecordWithWMS[] = skuBatchIdsToCreate
    .reduce((arr: RecordWithWMS[], skuBatchId: string): RecordWithWMS[] => {
      const skuBatchRecordFromAppDb: SkuBatchToSkuId | undefined = appData.find(
        (skuBatchToSkuId: SkuBatchToSkuId): boolean => skuBatchToSkuId.skuBatchId === skuBatchId,
      );

      if (!skuBatchRecordFromAppDb) {
        logger.error(`no records found in app SkuBatch [skuBatchId=${skuBatchId}}]`);
        badSkuBatchCounter.count += 1;
        return arr;
      }

      arr.push(...makeWarehouseRecordsForSkuBatchRecord(skuBatchRecordFromAppDb));
      return arr;
    }, []);

  logger.log(`created records [count=${records.length}, badSkuBatchRecordCount=${badSkuBatchCounter.count}]`);

  return records;
}

/**
 * Diffs the inventory between app SkuBatch and inventory to determine
 * what we need to copy over.
 */
export async function getDeltas(): Promise<string[]> {
  try {
    const inventorySkuBatchIds: Set<string> = new Set<string>(skuBatchIdsFromInventoryDb
        .map((r: { skuBatchId: string }) => r.skuBatchId));


    return [...new Set<string>(skuBatchIdsFromAppDb.map((r: { id: string }) => r.id))]
        .filter((x: string) => !inventorySkuBatchIds.has(x));
  } catch (err) {
    logger.error('error querying databases for skuBatchIds');
    logger.error(err);
    throw err;
  }
}

/**
 * Turns a delta into updated SkuBatchData
 * @param delta
 */
export const makeUpdates = (delta: skuBatchUpdate): SkuBatchData => {
  const entries = delta.updates.map((update) => [update.field, update.newValue]);
  const update = Object.fromEntries(entries);

  // Apply updates to the existing data and return the updated data
  return {
    skuBatchId: delta.skuBatchId,
    ...update,
  }
};

/**
 * Finds the deltas between two lists of SkuBatchData
 * @param appSkuBatchData
 * @param inventorySkuBatchData
 */
export const findDeltas = (
    appSkuBatchData: SkuBatchData[],
    inventorySkuBatchData: SkuBatchData[],
): skuBatchUpdate[] => {
  logger.log('finding data changes between inventory and app SkuBatch datasets');

  return appSkuBatchData
    .map((appSbd: SkuBatchData) => {
      const inventoryRecord: SkuBatchData | undefined = inventorySkuBatchData
          .find((r: SkuBatchData): boolean => r.skuBatchId == appSbd.skuBatchId);

      if (!inventoryRecord) {
        // if we cannot find the matching record, we have a problem
        logger.warn(`cannot find matching inventory record! [skuBatchId=${appSbd.skuBatchId}]`);
        // instead of throwing an error, return empty update array which will
        // get filtered out at the end of this chain
        return { skuBatchId: '', updates: [] };
      }

      // go through each key and see if it is different, if so, track it
      const updates: inventoryUpdate[] = Object.keys(inventoryRecord)
        .filter((k: string) => !['skuBatchId'].includes(k))
        .reduce((recordUpdates: inventoryUpdate[], key: string): inventoryUpdate[] => {
          const inventoryValue = inventoryRecord[key as keyof typeof inventoryRecord];
          const appValue = appSbd[key as keyof typeof appSbd];


        if (key === 'skuId' && inventoryValue != null) {
          // if the key is skuId and the current value is set, we won't update
            return recordUpdates;
        }

          if (inventoryValue != appValue) {
            recordUpdates.push({ field: key, newValue: appValue });
          }
          return recordUpdates;
        }, [] as inventoryUpdate[]);

      return {
        skuBatchId: inventoryRecord.skuBatchId,
        updates,
      };
    })
    .filter((sbu: skuBatchUpdate) => sbu.updates.length > 0);
};

/**
 * Finds changes in data between the app SkuBatch+Sku and inventory tables
 */
export async function findChangesBetweenDatasets(): Promise<SkuBatchData[]> {
  logger.log('finding app SkuBatch data that has changed and <> the inventory data');

  const updates: SkuBatchData[] = [appSkuBatchData].reduce(
    (accum: SkuBatchData[], inventorySkuBatchData: SkuBatchData[]): SkuBatchData[] => {
      const skuBatchIds: string[] = inventorySkuBatchData.map((sbd: SkuBatchData) => sbd.skuBatchId);

      logger.log(`querying Logistics.SkuBatch for data [skuBatchIdCount=${skuBatchIds.length}]`);
      // fetch SkuBatch+Sku data from the app database
      const appSkuBatchData: SkuBatchData[] = appSkuBatchDataForSkuBatchIds;

      // if we have a count mismatch, something is wrong, and we should log out a warning
      if (appSkuBatchData.length != inventorySkuBatchData.length) {
        // implement the logic to log a message with the IDs missing from app
        // data that exist in the inventory data
        const missingSkuBatchIds: string[] = appSkuBatchData
          .filter((sbd: SkuBatchData) => skuBatchIds.findIndex((id: string) => id === sbd.skuBatchId) < 0)
          .map((sbd: SkuBatchData) => sbd.skuBatchId);

        logger.warn("Warning! The following SkuBatches are present in Inventory db but missing from App db: " + missingSkuBatchIds.join(' '));
      }

      // push our new sql updates into the accumulator list
      const updatedData: SkuBatchData[] = findDeltas(appSkuBatchData, inventorySkuBatchData)
        .map((delta: skuBatchUpdate) => {
          const matchingAppSkuBatchData = appSkuBatchData.find((d: SkuBatchData) => d.skuBatchId === delta.skuBatchId);
          const update: SkuBatchData = makeUpdates(delta);
          return {
            ...matchingAppSkuBatchData,
            ...update,
          };
        });


      accum.push(...updatedData);
      return accum;
    },
    [] as SkuBatchData[],
  );

  logger.log(`built updates [count=${updates.length}]`);

  return updates;
}

/**
 * Updates inventory data from app SkuBatch and Sku
 */
export async function copyMissingInventoryRecordsFromSkuBatch(): Promise<void | Error> {
  logger.log('copying missing inventory records from app Sku/SkuBatch');

  // find out what skuBatchIds don't exist in inventory
  // [ 'sku-batch-id-5', sku-batch-id-6 ]
  const skuBatchIdsToCreate: string[] = await getDeltas();
  logger.log(`copying new skuBatch records... [skuBatchCount=${skuBatchIdsToCreate.length}]`);
  try {
    // Get full records, not just insert statements
    const records = await skuBatchToRecords(skuBatchIdsToCreate);

    const inventoryRequests: InventoryApiRequestBody[] = records.map((r: RecordWithWMS) => <InventoryApiRequestBody>(r))
    const inventoryAggregateRequests: InventoryAggregateApiRequestBody[] = records.map((r: RecordWithWMS) => <InventoryAggregateApiRequestBody>(r));

    // To avoid data desync, only proceed with happy path if all promises to both endpoints resolve successfully
    // Any error thrown by any call to either endpoint will trigger the error handling in the catch block below
    await Promise.all([
      PostInventory(inventoryRequests),
      PostInventoryAggregate(inventoryAggregateRequests),
    ]);

    logger.info(`Successfully added ${inventoryRequests.length} records to Inventory and ${inventoryAggregateRequests.length} records to Inventory Aggregate`);

  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log('done updating additive data to inventory from app db');
}

/**
 * Pulls inventory and SkuBatch data and finds changes in SkuBatch data
 * that are not in the inventory data.
 */
export async function updateInventoryDeltasFromSkuBatch(): Promise<void> {
  logger.log('updating inventory from deltas in "SkuBatch" data');

  try {
    const updates: SkuBatchData[] = await findChangesBetweenDatasets();

    const skuBatchIdsToUpdate: string[] = updates.map((u: SkuBatchData) => u.skuBatchId);
    const warehouseRecords: RecordWithWMS[] = await skuBatchToRecords(skuBatchIdsToUpdate);

    const inventoryUpdateRequests: InventoryApiRequestBody[] = warehouseRecords.map((record: RecordWithWMS) => {
      const updatedRecord: SkuBatchData | undefined = updates.find((u: SkuBatchData) => u.skuBatchId === record.skuBatchId);
      return updatedRecord ? <InventoryApiRequestBody>({ ...record, ...updatedRecord}) : <InventoryApiRequestBody>({ ...record });
    });

    const inventoryAggregateUpdateRequests: InventoryAggregateApiRequestBody[] = warehouseRecords.map((record: RecordWithWMS) => {
      const updatedRecord: SkuBatchData | undefined = updates.find((u: SkuBatchData) => u.skuBatchId === record.skuBatchId);
      return updatedRecord ? <InventoryAggregateApiRequestBody>({ ...record, ...updatedRecord}) : <InventoryAggregateApiRequestBody>({ ...record });
    });

    // To avoid data desync, only proceed with happy path if all promises to both endpoints resolve successfully
    // Any error thrown by any API request to either endpoint will trigger the error handling in the catch block below
    await Promise.all([
      PutInventory(inventoryUpdateRequests),
      PutInventoryAggregate(inventoryAggregateUpdateRequests),
    ]);

    logger.info(`Successfully updated ${inventoryUpdateRequests.length} records in Inventory and ${inventoryAggregateUpdateRequests.length} records in Inventory Aggregate`);

    
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log('done updating inventory from deltas from app db');
}

/**
 * Primary entry point to sync SkuBatch data from the app
 * database over to the inventory database
 */
export async function sync(): Promise<void | Error> {
  try {
    await copyMissingInventoryRecordsFromSkuBatch();
    await updateInventoryDeltasFromSkuBatch();
  } catch (err) {
    logger.error('error syncing skuBatch data');
    return Promise.reject(err);
  }
}