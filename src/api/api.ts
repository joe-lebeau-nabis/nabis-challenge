import axios, { AxiosResponse } from "axios"
import { InventoryAggregateApiRequestBody, InventoryApiRequestBody, RecordWithWMS } from "src/interfaces.util";

const logger = console;
const baseUrl = "https://local-inventory.nabis.dev/v1";
export const inventoryUrl = `${baseUrl}/inventory`
export const inventoryAggregateUrl = `${baseUrl}/inventory-aggregate`

/**
 * Assumptions made in the implementation of these API calls:
 * - No handling of the POST/PUT response body is necessary, only success vs error response
 * - The docs do not specify if the API endpoints can accept arrays of records vs single records, so
 * the implementations below assume the endpoints accept single-record POSTs and PUTs, and use
 * Promise.all to handle the multiple async API calls
 */

/**
 * Posts a batch of records to the Inventory endpoint
 * @param data 
 */
export async function PostInventory(data: InventoryApiRequestBody[]): Promise<void> {
  try {
    const promises: Promise<AxiosResponse>[] = data.map((body: InventoryApiRequestBody) => axios.post(inventoryUrl, body));
    await Promise.all(promises);

  } catch (err) {
    logger.error(err);
    throw err;
  }
}

/**
 * Updates a batch of records to the Inventory endpoint
 * @param data 
 */
export async function PutInventory(data: InventoryApiRequestBody[]): Promise<void> {
  try {
    const promises: Promise<AxiosResponse>[] = data.map((body: InventoryApiRequestBody) => axios.put(inventoryUrl, body));
    await Promise.all(promises);

  } catch (err) {
    logger.error(err);
    throw err;
  }
}

/**
 * Posts a batch of records to the Inventory Aggregate endpoint
 * @param data 
 */
export async function PostInventoryAggregate(data: InventoryAggregateApiRequestBody[]): Promise<void> {
  try {
    const promises: Promise<AxiosResponse>[] = data.map((body: InventoryAggregateApiRequestBody) => axios.post(inventoryAggregateUrl, body));
    await Promise.all(promises);

  } catch (err) {
    logger.error(err);
    throw err;
  }
}

/**
 * Updates a batch of records to the Inventory Aggregate endpoint
 * @param data 
 */
export async function PutInventoryAggregate(data: InventoryAggregateApiRequestBody[]): Promise<void> {
  try {
    const promises: Promise<AxiosResponse>[] = data.map((body: InventoryAggregateApiRequestBody) => axios.put(inventoryUrl, body));
    await Promise.all(promises);

  } catch (err) {
    logger.error(err);
    throw err;
  }
}
